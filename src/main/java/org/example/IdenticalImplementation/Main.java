package org.example.IdenticalImplementation;


public class Job {
    public int basePriority;
    private int[] priorities;

    //@ inline resource prioritiesPerm() = Perm(this.priorities, write) ** this.priorities != null ** this.priorities.length >= 1 ** Perm(this.priorities[*], write);
    //@ inline resource basePriorityPerm() = Perm(this.basePriority, write);
    //@ inline resource fullPerm() = basePriorityPerm() ** prioritiesPerm();


    /*@
        requires priority >= 0;
        ensures this.fullPerm();
        ensures this.basePriority == priority;
        ensures this.priorities.length == 1;
        ensures this.priorities[0] == priority;
     @*/
    public Job(int priority) {
        this.priorities = new int[]{priority};
        this.basePriority = priority;
    }

    /*@
        context_everywhere this.prioritiesPerm();
        context_everywhere (\forall int i; 0 <= i && i < this.priorities.length; this.priorities[i] >= 0);
        context this.priorities[0] >= 0;
        ensures \result >= 0;
        ensures (\exists int i; 0 <= i && i < this.priorities.length; \result == this.priorities[i]);
    @*/
    public int getPriority() {
        int min = this.priorities[0];

        /*@
            loop_invariant 1 <= j && j <= this.priorities.length;
            loop_invariant (\forall int i; 0 <= i && i < this.priorities.length; this.priorities[i] >= 0);
            loop_invariant (\exists int i; 0 <= i && i < this.priorities.length; min == this.priorities[i]);
        @*/
        for (int j = 1; j < this.priorities.length; j++) {
            if (this.priorities[j] < min) {
                min = this.priorities[j];
            }
        }
        return min;
    }

    /*@
        context this.prioritiesPerm();
        ensures this.priorities.length == \old(this.priorities.length) + 1;
        ensures this.priorities[this.priorities.length -1] == priority;
        ensures (\forall int i; 0 <= i && i < \old(this.priorities.length); this.priorities[i] == \old(this.priorities[i]));
    @*/
    public void addPriority(int priority) {
        int[] temp = new int[priorities.length + 1];
        /*@
            loop_invariant Perm(this.priorities, write) ** this.priorities != null;
            loop_invariant this.priorities.length == temp.length - 1;
            loop_invariant (\forall* int i; 0 <= i && i < this.priorities.length; Perm(this.priorities[i], 1\2) ** this.priorities[i] == \old(this.priorities[i]));
            loop_invariant (\forall* int i; 0 <= i && i < temp.length; Perm(temp[i], write));
            loop_invariant 0 <= i && i <= this.priorities.length && i <= temp.length;
            loop_invariant (\forall int j; 0 <= j && j < i; temp[j] == \old(this.priorities[j]));
        @*/
        for (int i = 0; i < priorities.length; i++) {
            temp[i] = priorities[i];
        }
        //@ assert (\forall int j; 0 <= j && j < priorities.length; temp[j] == \old(this.priorities[j]));
        temp[priorities.length] = priority;
        this.priorities = temp;
    }


    /*@
        context_everywhere this.prioritiesPerm();
        context priority >= 0;
        requires this.priorities.length >= 2;
        requires (\exists int i; 0 <= i && i < this.priorities.length; this.priorities[i] == priority);
        ensures this.priorities.length == \old(this.priorities.length) - 1;

    @*/
    public void removePriority(int priority) {
        int[] temp = new int[priorities.length - 1];
        int offset = 0;
        /*@
            loop_invariant this.priorities.length - 1 == temp.length;
            loop_invariant \old(this.priorities.length) >= 2;
            loop_invariant (\forall* int i; 0 <= i && i < temp.length; Perm(temp[i], write));
            loop_invariant (\exists int i; 0 <= i && i < \old(this.priorities.length); \old(this.priorities[i]) == priority);
            loop_invariant 0 <= j && j <= \old(this.priorities.length);
            loop_invariant 0 <= offset && offset <= 1;
            loop_invariant 0 <= (j-offset) && (j-offset) <= temp.length && (j-offset) <= \old(this.priorities.length) -1;
         @*/
        for (int j = 0; j < priorities.length && j-offset < temp.length; j++) {
            if (priority == priorities[j] && offset == 0) {
                offset = 1;
            }else {
                temp[j-offset] = priorities[j];
            }
        }
        this.priorities = temp;
    }
}


public class Mutex {
    private Job owner;
    private int nested = 0;
    private Job[] accessors;

    //@ inline resource accessorsPerm() = Perm(this.accessors, write) ** this.accessors != null ** Perm(this.accessors[*], write);
    //@ inline resource ownerPerm() = Perm(this.owner, write);
    //@ inline resource nestedPerm() = Perm(this.nested, write) ** this.nested >= 0;
    //@ inline resource fullPerm() = accessorsPerm() ** ownerPerm() ** nestedPerm();

    //@ inline resource accessorBasePriorityPerm() = (\forall int i, int j; 0 <= i && i < this.accessors.length && 0 <= j && j < this.accessors.length; i != j ==> this.accessors[i] != this.accessors[j]) ** (\forall* int i; 0 <= i && i < this.accessors.length; this.accessors[i].basePriorityPerm()) ** (\forall int i; 0 <= i && i < this.accessors.length; this.accessors[i].basePriority >= 0);


    /*@
        ensures accessorsPerm();
     */
    public Mutex() {
        accessors = new Job[0];
    }


    /*@
        yields int resultPriorityCeiling;
        context_everywhere accessorsPerm();
        context_everywhere accessorBasePriorityPerm();
        ensures this.accessors == \old(this.accessors);
        ensures \result >= -1;
        ensures \old(this.accessors.length) == 0 ==> \result == -1;
        ensures \old(this.accessors.length) > 0 ==> \result >=0;
     */
    public int getPriorityCeiling() {
        int min = -1;
        /*@
            loop_invariant 0 <= i && i <= this.accessors.length;
            loop_invariant this.accessors == \old(this.accessors);
            loop_invariant min >= -1;
            loop_invariant this.accessors.length == 0 ==> min == -1;
            loop_invariant this.accessors.length > 0 && i > 0 ==> min >=0;
            loop_invariant this.accessors.length > 0 && i == 0 ==> min == -1;
         */
        for (int i = 0; i < this.accessors.length; i++) {
            Job myjob = this.accessors[i];
            if (myjob.basePriority < min || min == -1) {
                min = myjob.basePriority;
            }
        }

        //@ghost int resultPriorityCeiling = min;
        return min;
    }

    /*@
        context ownerPerm();
        ensures this.owner == newOwner;
    @*/
    public void setOwner(Job newOwner) {
        this.owner = newOwner;
    }

    /*@
        context ownerPerm();
        ensures \result == this.owner;
    @*/
    public Job getOwner() {
        return this.owner;
    }

    /*@
        context nestedPerm();
        ensures this.nested == \old(this.nested) + 1;
    @*/
    public void incrementNested() {
        this.nested++;
    }

    /*@
        context nestedPerm();
        requires this.nested > 0;
        ensures this.nested == \old(this.nested) - 1;
    @*/
    public void decrementNested() {
        this.nested--;
    }

    /*@
        context nestedPerm();
        ensures \result == this.nested;
    @*/
    public int getNestLevel() {
        return nested;
    }

    /*@
        context_everywhere accessorsPerm();
        ensures this.accessors.length == \old(this.accessors.length) + 1;
        ensures this.accessors[this.accessors.length - 1] == myJob;
    @*/
    public void addAccessor(Job myJob) {
        Job[] temp = new Job[accessors.length + 1];
        /*@
            loop_invariant this.accessors.length == temp.length - 1;
            loop_invariant (\forall* int i; 0 <= i && i < temp.length; Perm(temp[i], write));
            loop_invariant 0 <= i && i <= this.accessors.length && i <= temp.length - 1;
        @*/
        for (int i = 0; i < accessors.length; i++) {
            temp[i] = accessors[i];
        }
        temp[accessors.length] = myJob;
        this.accessors = temp;
    }

    /*@
        context_everywhere accessorsPerm();
        context myJob != null;
        ensures \result == true ==> (\exists int i; 0 <= i && i < this.accessors.length; this.accessors[i] == myJob);
    @*/
    public boolean hasJob(Job myJob) {
        /*@
            loop_invariant this.accessors != null;
            loop_invariant 0 <= i && i <= this.accessors.length;
         */
        for (int i = 0; i < this.accessors.length; i++) {
            if (accessors[i] == myJob) {
                return true;
            }
        }
        return false;
    }
}


public class Scheduler {
    public static final String STATUS_MUTEX_CEILING_VIOLATED = "MUTEX_CEILING_VIOLATED";
    public static final String STATUS_SUCCESSFUL = "SUCCESSFUL";
    public static final String STATUS_NOT_OWNER = "NOT_OWNER";


    //@ ghost int mutexPriorityCeiling= -1;


    /*@
        context mutex.accessorsPerm();
        context owner.fullPerm();
        context mutex.accessorBasePriorityPerm();
        context Perm(mutexPriorityCeiling,write);
        context mutex != null && owner != null;
        context mutex.ownerPerm();
        requires (\forall int i; 0 <= i && i < owner.priorities.length; owner.priorities[i] >= 0);
        ensures \result == STATUS_SUCCESSFUL || \result == STATUS_NOT_OWNER || \result == STATUS_MUTEX_CEILING_VIOLATED;
        ensures owner.basePriority < mutexPriorityCeiling ==> \result == STATUS_MUTEX_CEILING_VIOLATED;
        ensures owner.basePriority >= mutexPriorityCeiling ==> \result == STATUS_SUCCESSFUL;
        ensures owner.basePriority >= mutexPriorityCeiling ==> mutex.owner == owner;
    @*/
    public String _CORE_ceiling_mutex_Set_owner(Mutex mutex, Job owner) {
        int mutexCeiling = mutex.getPriorityCeiling();
        //@ ghost mutexPriorityCeiling = mutexCeiling;
        int ownerPriority = owner.basePriority;
//        int ownerPriority = owner.getPriority(); //Mistake of paper
        //@assert mutexPriorityCeiling == mutexCeiling;
        if (ownerPriority < mutexCeiling) {
            return STATUS_MUTEX_CEILING_VIOLATED;
        }

        mutex.setOwner(owner);
        owner.addPriority(mutex.getPriorityCeiling());
        return STATUS_SUCCESSFUL;
    }


    //@ ghost Job mutexOwner = null;
    /*@
        context mutex.accessorsPerm();
        context mutex.accessorBasePriorityPerm();
        context mutex != null && executing != null;
        context mutex.ownerPerm();
        context executing.fullPerm();
        context Perm(mutexPriorityCeiling,write);
        context Perm(mutexOwner,write);
        requires (\forall int i; 0 <= i && i < executing.priorities.length; executing.priorities[i] >= 0);
        context Perm(mutex.nested, write);
        requires mutex.nested >= 0;
        ensures \result == STATUS_SUCCESSFUL || \result == STATUS_MUTEX_CEILING_VIOLATED || \result == STATUS_NOT_OWNER;
        ensures mutexOwner == null ==> \result == STATUS_SUCCESSFUL || \result == STATUS_MUTEX_CEILING_VIOLATED;
        ensures mutexOwner != null ==> mutexOwner == executing ==> \result == STATUS_SUCCESSFUL;
        ensures mutexOwner != null ==> mutexOwner != executing ==> \result == STATUS_NOT_OWNER;
        ensures mutexOwner != null ==> mutexOwner != executing ==> mutex.nested == \old(mutex.nested);
        ensures mutexOwner != null ==> mutexOwner == executing ==> mutex.nested == \old(mutex.nested) + 1;
        ensures mutexOwner == null ==> mutex.nested == \old(mutex.nested);
    @*/
    public String _CORE_ceiling_mutex_Seize(Mutex mutex, Job executing) {
        Job owner = mutex.getOwner();
        //@ ghost mutexOwner = owner;

        if (owner == null) {
            String status = _CORE_ceiling_mutex_Set_owner(mutex, executing);
            //@assert status == STATUS_SUCCESSFUL || status == STATUS_MUTEX_CEILING_VIOLATED;
            return status;
        }
        //@ assert owner != null;
        if (owner == executing) {
            //@ assert owner == executing;
            mutex.incrementNested();
            //@ assert mutex.nested > 0;
            return STATUS_SUCCESSFUL;
        }
        return STATUS_NOT_OWNER;
    }



    /*@
        context mutex.fullPerm();
        context mutex.accessorBasePriorityPerm();
        context mutex != null && executing != null;
        context executing.fullPerm();
        context Perm(mutexPriorityCeiling,write);
        requires (\forall int i; 0 <= i && i < executing.priorities.length; executing.priorities[i] >= 0);
        requires mutex.accessors.length > 0;
        requires executing.priorities.length > 2;
        ensures \result == STATUS_SUCCESSFUL || \result == STATUS_NOT_OWNER;
        ensures \result == STATUS_NOT_OWNER ==> \old(mutex.owner) == null || mutex.owner != executing;
    @*/
    public String _CORE_ceiling_mutex_Surrender(Mutex mutex, Job executing) {
        if (mutex.getOwner() == null || mutex.getOwner() != executing) {
            return STATUS_NOT_OWNER;
        }

        int nest_level = mutex.getNestLevel();
        if (nest_level > 0) {
            mutex.decrementNested();
            //@ assert mutex.nested >= 0;
            return STATUS_SUCCESSFUL;
        }
        int priorityCeiling = mutex.getPriorityCeiling();
        //@ ghost mutexPriorityCeiling = priorityCeiling;
        //@ assume (\exists int i; 0 <= i && i < executing.priorities.length; executing.priorities[i] == priorityCeiling);
        //@ assert priorityCeiling >= 0;
        executing.removePriority(priorityCeiling);
        return STATUS_SUCCESSFUL;
    }

}
